from django.shortcuts import render
import json
# Create your views here.

def load_db():
    with open("./db.json") as f:
        like_db = json.load(f)
    return like_db

def index(request):
    return render(request,'index.html')

def api(request):
    like_db = load_db()
    if 'id' in request.GET:
        if request.GET['id'] in like_db:
            like_db[request.GET['id']] = int(like_db[request.GET['id']]) + 1
        else:
            like_db[request.GET['id']] = 1
        with open('./db.json', 'w') as fp:
            json.dump(like_db, fp)
        response = {'likes' : like_db}
    else:
        response = {'likes' : like_db[request.GET['q']]}
    return render(request,'api.html',response)