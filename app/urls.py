from django.urls import path
from django.conf.urls import include
from .views import *
from django.conf import settings
from . import views

urlpatterns = [
    path('', index, name='index'),
    path('index.html', index, name='index'),
    path('api', api, name='api'),
    ]