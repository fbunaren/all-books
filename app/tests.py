from django.test import TestCase, LiveServerTestCase
from django.test import Client
import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
from django.apps import apps
import urllib.request
from time import sleep

# Create your tests here.
class AppTest(TestCase):
    def test_index_response(self):
        response = Client().get('/index.html')
        self.assertEqual(response.status_code,200)
    
    def test_index_template(self):
        response = Client().get('/index.html')
        self.assertTemplateUsed(response,'index.html')

    def test_api(self):
        # Check if data not found in Database
        response = self.client.get('/api?id=123123123')  
        # Check if data can be stored in Database
        response = self.client.get('/api?id=123')        
        # Check if data can be re-stored in Database
        response = self.client.get('/api?id=123')        
        # Check if we can get the data from database
        response = self.client.get('/api?q=123')        
        self.assertTrue(response.content.decode('utf8') != None)

class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        super().setUp()
        self.link = self.live_server_url# "http://fbunaren.herokuapp.com" #
        opt = webdriver.ChromeOptions()
        opt.add_argument('--no-sandbox')
        opt.add_argument('--headless')
        opt.add_argument('--disable-gpu')
        opt.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome(chrome_options=opt,executable_path='chromedriver')

    def tearDown(self):
        self.selenium.refresh()
        self.selenium.quit()
        super().tearDown()

    def test_index_loaded(self):
        self.selenium.get(self.link)
        #Test if the Webpage is Loaded
        #Test h1 Header
        self.assertTrue(self.selenium.find_elements_by_tag_name('h1') != None)
        #Test if book table is loaded
        self.assertTrue(self.selenium.find_element_by_tag_name('table') != False)

    def test_find_book(self):
        self.selenium.get(self.link)
        # Type and click on search box
        self.selenium.find_element_by_id("q").send_keys("THE LORD OF HEAVEN GLORIA VALERIE LIM")
        self.selenium.find_element_by_id("search").click()
        sleep(1)
        # Test if it loads more than one row (which means that there is book found)
        self.assertTrue(len(self.selenium.find_elements_by_tag_name("tr")) > 1)

    def test_like(self):
        self.selenium.get(self.link)
        # Type and click on search box
        self.selenium.find_element_by_id("q").send_keys("THE LORD OF HEAVEN GLORIA VALERIE LIM")
        self.selenium.find_element_by_id("search").click()
        sleep(1)
        self.assertTrue(len(self.selenium.find_elements_by_name("like_btn")) > 1)
        self.assertTrue(len(self.selenium.find_elements_by_name("like_count")) > 1)
        # Save previous likes
        prev_likes = self.selenium.find_elements_by_name("like_count")[0].text
        print(self.selenium.find_elements_by_name("like_btn")[0].text)
        # Click on like button, check if the likes increases
        self.selenium.find_elements_by_name("like_btn")[0].click()
        # Check if the likes increases
        self.assertGreater(self.selenium.find_elements_by_name("like_count")[0].text,prev_likes)

    def test_top_books(self):
        self.selenium.get(self.link)
        # Type and click on search box
        self.selenium.find_element_by_id("q").send_keys("THE LORD OF HEAVEN GLORIA VALERIE LIM")
        self.selenium.find_element_by_id("search").click()
        sleep(1)
        tmp = self.selenium.find_elements_by_name("like_btn")
        for i in tmp:
            i.click()
        sleep(1)
        #Check if the modal button can be clicked
        self.assertTrue(self.selenium.find_element_by_id("btn_top5") != None)
        